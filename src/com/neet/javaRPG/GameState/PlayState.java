package com.neet.javaRPG.GameState;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;

import com.neet.MapViewer.Main.MapMain;
import com.neet.javaRPG.Entity.Enemy;
import com.neet.javaRPG.Entity.Item;
import com.neet.javaRPG.Entity.Player;
import com.neet.javaRPG.HUD.Hud;
import com.neet.javaRPG.Main.GamePanel;
import com.neet.javaRPG.Manager.GameStateManager;
import com.neet.javaRPG.Manager.Keys;
import com.neet.javaRPG.RPG.Skill;
import com.neet.javaRPG.RPG.ImplementSkill.PowerAttack;
import com.neet.javaRPG.TileMap.TileMap;

public class PlayState extends GameState {
	
	// player
	private Player player;
	
	// tilemap
	private TileMap tileMap;
	
	// enemies
	private ArrayList<Enemy> enemies;
	private Enemy fightingEnemy;
	
	// items
	private ArrayList<Item> items;
	
	// sparkles
	private boolean isWin;
	
	// camera position
	private int xsector;
	private int ysector;
	private int sectorSize; 
	
	// hud
	private Hud hud;
	
	// transition box
	private ArrayList<Rectangle> boxes;
	
	public PlayState(GameStateManager gsm) {
		super(gsm);
	}
	
	public void load(TileMap tm,
			Player player, 
			ArrayList<Enemy> enemies,
			ArrayList<Item> items) {
		this.tileMap = tm;
		this.player = player;
		this.enemies = enemies;
		this.items = items;
		
		sectorSize = GamePanel.WIDTH;
		xsector = player.getx() / sectorSize;
		ysector = player.gety() / sectorSize;
		tileMap.setPositionImmediately(-xsector * sectorSize, -ysector * sectorSize);
	}
	
	public void init() {
		
		// create lists
		enemies = new ArrayList<Enemy>();
		items = new ArrayList<Item>();
		
		// load map
		tileMap = new TileMap(16);
		tileMap.loadTiles("/Resources/Tilesets/testtileset.gif");
		tileMap.loadMap("/Resources/Maps/testmap.map");
		
		// create player
		player = new Player(tileMap);
		player.addSkill(new PowerAttack(player.getATK()));
		
		// fill lists
		populateEnemies();
		populateItems();
		
		// initialize player
		player.setTilePosition(17, 17);
		
		// set up camera position
		sectorSize = GamePanel.WIDTH;
		xsector = player.getx() / sectorSize;
		ysector = player.gety() / sectorSize;
		tileMap.setPositionImmediately(-xsector * sectorSize, -ysector * sectorSize);
		
		// load hud
		hud = new Hud(player, enemies);
				
		// start event
		boxes = new ArrayList<Rectangle>();
		
		isWin = false;
	}
	
	private void populateEnemies() {
		
		Enemy d;
		List<Skill> skillList = new ArrayList<>();
		skillList.add(new PowerAttack(5));
		
		d = new Enemy(tileMap);
		d.setTilePosition(20, 20);
		d.addChange(new int[] { 23, 19, 1 });
		d.addChange(new int[] { 23, 20, 1 });
		d.setSkillList(skillList);
		enemies.add(d);
		d = new Enemy(tileMap);
		d.setTilePosition(12, 36);
		d.addChange(new int[] { 31, 17, 1 });
		d.setSkillList(skillList);
		enemies.add(d);
		d = new Enemy(tileMap);
		d.setTilePosition(28, 4);
		d.addChange(new int[] {27, 7, 1});
		d.addChange(new int[] {28, 7, 1});
		d.setSkillList(skillList);
		enemies.add(d);
		d = new Enemy(tileMap);
		d.setTilePosition(4, 34);
		d.addChange(new int[] { 31, 21, 1 });
		d.setSkillList(skillList);
		enemies.add(d);
		
		d = new Enemy(tileMap);
		d.setTilePosition(28, 19);
		d.setSkillList(skillList);
		enemies.add(d);
		d = new Enemy(tileMap);
		d.setTilePosition(35, 26);
		d.setSkillList(skillList);
		enemies.add(d);
		d = new Enemy(tileMap);
		d.setTilePosition(20, 30);
		d.setSkillList(skillList);
		enemies.add(d);
		d = new Enemy(tileMap);
		d.setTilePosition(27, 28);
		d.setSkillList(skillList);
		enemies.add(d);
		d = new Enemy(tileMap, "Boss", 100, 100, 100, 100, 1, null);
		d.setSkillList(skillList);
		d.setTilePosition(38, 36);
		enemies.add(d);
		d = new Enemy(tileMap);
		d.setTilePosition(14, 25);
		d.setSkillList(skillList);
		enemies.add(d);
		d = new Enemy(tileMap);
		d.setTilePosition(4, 21);
		d.setSkillList(skillList);
		enemies.add(d);
		d = new Enemy(tileMap);
		d.setTilePosition(9, 14);
		d.setSkillList(skillList);
		enemies.add(d);
		d = new Enemy(tileMap);
		d.setTilePosition(4, 3);
		d.setSkillList(skillList);
		enemies.add(d);
		d = new Enemy(tileMap);
		d.setTilePosition(20, 14);
		d.setSkillList(skillList);
		enemies.add(d);
		d = new Enemy(tileMap);
		d.setTilePosition(13, 20);
		d.setSkillList(skillList);
		enemies.add(d);
		
	}
	
	//Add boat
	private void populateItems() {
		
		Item item;
	
		item = new Item(tileMap);
		item.setType(Item.BOAT);
		
		if (MapMain.viewerLaunch == false) {
			item.setTilePosition(12, 4);
		}
		else if (MapMain.tileMapViewer.getBoatRow() == -1 && MapMain.tileMapViewer.getBoatCol() == -1) {
			item.setTilePosition(12, 4);	
		}
		else {
			item.setTilePosition(MapMain.tileMapViewer.getBoatRow(), MapMain.tileMapViewer.getBoatCol());
		}
		items.add(item);
		
	}
	
	public void update() {
		handleInput();
		
		if(enemies.isEmpty()) {
			isWin = true;
			finish();
		}
		
		// update camera
		int oldxs = xsector;
		int oldys = ysector;
		xsector = player.getx() / sectorSize;
		ysector = player.gety() / sectorSize;
		tileMap.setPosition(-xsector * sectorSize, -ysector * sectorSize);
		tileMap.update();
				
		if(tileMap.isMoving()) return;
		
		// update player
		player.update();
		
		// update enemies
		for(int i = 0; i < enemies.size(); i++) {
			
			Enemy d = enemies.get(i);
			d.update();
			
			if(player.intersects(d)) {
				enemies.remove(i);
				i--;
				
				fightingEnemy = d;
				gsm.setCombat(true);
			}
		}
				
		// update items
		for(int i = 0; i < items.size(); i++) {
			Item item = items.get(i);
			if(player.intersects(item)) {
				items.remove(i);
				i--;
				item.collected(player);
			}
		}
		
	}
	
	public void draw(Graphics2D g) {
		
		// draw tilemap
		tileMap.draw(g);
		
		// draw player
		player.draw(g);
		
		// draw enemies
		for(Enemy d : enemies) {
			d.draw(g);
		}
		
		// draw items
		for(Item i : items) {
			i.draw(g);
		}
		
		// draw hud
		hud.draw(g);
		
		// draw transition boxes
		g.setColor(java.awt.Color.BLACK);
		for(int i = 0; i < boxes.size(); i++) {
			g.fill(boxes.get(i));
		}
		
	}
	
	public void handleInput() {
		if(Keys.isPressed(Keys.ESCAPE)) {
			gsm.setPaused(true);
		}
		
		if(Keys.isDown(Keys.LEFT)) player.setLeft();
		if(Keys.isDown(Keys.RIGHT)) player.setRight();
		if(Keys.isDown(Keys.UP)) player.setUp();
		if(Keys.isDown(Keys.DOWN)) player.setDown();
	}
	
	public void finish() {
		gsm.setState(gsm.GAMEOVER);
		gsm.setIsWin(isWin);
	}
	
	public TileMap getTileMap() {
		return tileMap;
	}
	
	public void playerDead() {
		isWin = false;
		finish();
	}
	
	public void enemyDefeat() {
		enemies.remove(fightingEnemy);
		
		// make any changes to tile map
		ArrayList<int[]> ali = fightingEnemy.getChanges();
		for(int[] j : ali) {
			tileMap.setTile(j[0], j[1], j[2]);
		}
		player.increaseXP(fightingEnemy);
		if(player.canLevelUp()) {
			player.levelUp();
			player.changeSkill(0, new PowerAttack(player.getATK()));
		}
			
	}
	
	public Player getPlayer() {
		return this.player;
	}
	
	
	
	public Enemy getEnemy() {
		return fightingEnemy;
	}
}
